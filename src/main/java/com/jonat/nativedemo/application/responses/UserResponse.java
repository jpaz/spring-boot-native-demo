package com.jonat.nativedemo.application.responses;

public record UserResponse(String id, String email) {
}
