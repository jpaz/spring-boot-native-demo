package com.jonat.nativedemo.application.services;

import com.jonat.nativedemo.application.requests.UserCreateRequest;
import com.jonat.nativedemo.application.responses.UserResponse;

import java.util.List;

public interface UserService {

    UserResponse createUser(UserCreateRequest userCreateRequest);

    List<UserResponse> findAll();

}
