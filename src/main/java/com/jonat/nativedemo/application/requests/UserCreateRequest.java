package com.jonat.nativedemo.application.requests;

public record UserCreateRequest(String email, String password) {
}
