package com.jonat.nativedemo.domain.services;

import com.jonat.nativedemo.application.exceptions.EmailAlreadyExists;
import com.jonat.nativedemo.application.requests.UserCreateRequest;
import com.jonat.nativedemo.application.responses.UserResponse;
import com.jonat.nativedemo.application.services.UserService;
import com.jonat.nativedemo.domain.entities.UserEntity;
import com.jonat.nativedemo.domain.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private static UserResponse toUserResponse(UserEntity user) {
        return new UserResponse(user.getId(), user.getEmail());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserResponse createUser(UserCreateRequest userCreateRequest) {
        if (userRepository.existsByEmail(userCreateRequest.email())) {
            throw new EmailAlreadyExists();
        }

        var user = new UserEntity();
        user.setId(UUID.randomUUID().toString());
        user.setEmail(userCreateRequest.email());
        user.setPassword(userCreateRequest.password());

        userRepository.save(user);

        return toUserResponse(user);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserResponse> findAll() {
        return userRepository.findAll().stream()
                .map(UserServiceImpl::toUserResponse)
                .toList();
    }
}
