package com.jonat.nativedemo.domain.repositories;

import com.jonat.nativedemo.domain.entities.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<UserEntity, String> {

    boolean existsByEmail(String email);

}
