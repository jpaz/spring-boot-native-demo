package com.jonat.nativedemo.unit;

import com.jonat.nativedemo.application.controllers.UserController;
import com.jonat.nativedemo.application.requests.UserCreateRequest;
import com.jonat.nativedemo.application.responses.UserResponse;
import com.jonat.nativedemo.application.services.UserService;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UserControllerTest {

    @RequiredArgsConstructor
    static class UserServiceMock implements UserService {
        public Function<UserCreateRequest, UserResponse> onCreateUser;
        public Supplier<List<UserResponse>> onFindAllUsers;

        @Override
        public UserResponse createUser(UserCreateRequest userCreateRequest) {
            return onCreateUser.apply(userCreateRequest);
        }

        @Override
        public List<UserResponse> findAll() {
            return onFindAllUsers.get();
        }
    }

    @Test
    void whenCreateUserThenUserCreated() {
        var userServiceMock = new UserServiceMock();
        userServiceMock.onCreateUser = (request) -> new UserResponse(UUID.randomUUID().toString(), request.email());

        var controller = new UserController(userServiceMock);

        var request = new UserCreateRequest("john", "john@example.com");
        var response = controller.createUser(request);

        assertNotNull(response);
        assertNotNull(response.id());
        assertEquals(request.email(), response.email());
    }

    @Test
    void whenFindAllUsersThenUsers() {
        var users = List.of(new UserResponse("john", "john@example.com"));
        var userServiceMock = new UserServiceMock();
        userServiceMock.onFindAllUsers = () -> users;

        var controller = new UserController(userServiceMock);

        var response = controller.findAll();

        assertNotNull(response);
        assertFalse(response.isEmpty());
        assertArrayEquals(users.toArray(), response.toArray());
    }

}
